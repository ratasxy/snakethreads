#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "includes.h"
#include "painter.h"
#include "snake.h"
#include <mutex>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onAddPoint(int x, int y);
    void onDelPoint(int x, int y);
    
private slots:
    void on_btnStart_clicked();
    void on_btnStop_clicked();

private:
    Painter * painter;
    QList<Snake*> threads;
    Ui::MainWindow *ui;
    map<int,map<int,mutex> > * board;
};

#endif // MAINWINDOW_H
