#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QThread"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->gView->dragMode();

    QGraphicsView ** t = &ui->gView;

    this->painter = new Painter(t);

    //this->painter->addPoint(make_pair(10,10));

    this->board = new map<int, map<int, mutex> >;

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnStart_clicked()
{
    int size = ui->spbNumber->value();
    for(int i=0;i<size;i++)
    {
        threads.append(new Snake(this->board,this));
        connect(threads[i],SIGNAL(addPoint(int, int)), this, SLOT(onAddPoint(int, int)));
        connect(threads[i],SIGNAL(delPoint(int, int)), this, SLOT(onDelPoint(int, int)));
    }

    for(int i=0;i<this->threads.size();i++)
    {
        this->threads[i]->start();
    }
}

void MainWindow::on_btnStop_clicked()
{


}

void MainWindow::onAddPoint(int x, int y)
{
    this->painter->addPoint(make_pair(x, y));
}

void MainWindow::onDelPoint(int x, int y)
{
    this->painter->delPoint(make_pair(x, y));
}
