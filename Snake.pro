#-------------------------------------------------
#
# Project created by QtCreator 2014-06-19T09:02:03
#
#-------------------------------------------------

QT       += core gui
QT += opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Snake
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    painter.cpp \
    snake.cpp

HEADERS  += mainwindow.h \
    painter.h \
    includes.h \
    snake.h

FORMS    += mainwindow.ui

CONFIG   += console
CONFIG += c++11
