#ifndef PAINTER_H
#define PAINTER_H

#include "includes.h"
#include "ui_mainwindow.h"

class Painter
{
public:
    Painter(QGraphicsView **);
    bool addPoint(Point p);
    bool delPoint(Point p);
private:
    QGraphicsView ** visor;
    QGraphicsScene * scene;
};

#endif // PAINTER_H
