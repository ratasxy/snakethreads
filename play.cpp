#include "play.h"

Play::Play(int nSnakes, Painter * &painter)
{
    //this->painter = painter;
    for(int i=0;i<nSnakes;i++)
    {
        this->snakes.push_back(new Snake(6, painter));
    }
}

void Play::run()
{
    while(true)
    {
        for(vector<Snake *>::iterator i=this->snakes.begin(); i!=this->snakes.end(); i++)
        {
            (*i)->forward();
        }
        this->msleep(100);
    }

}
