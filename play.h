#ifndef PLAY_H
#define PLAY_H

#include "QThread"
#include "snake.h"
#include "painter.h"

class Play : public QThread
{
    Q_OBJECT
public:
    Play(int nSnakes, Painter * &);
    void run();
private:
    Painter * painter;
    vector<Snake *> snakes;
};

#endif // PLAY_H
