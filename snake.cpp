#include "snake.h"
#include <iostream>
Snake::Snake(map<int,map<int,mutex> > * &board, QObject *parent) :
    QThread(parent)
{
    this->size = 6;
    int xr = rand()%640, yr= rand()%490;
    Point origin = make_pair(xr - xr%10,xr- xr%10);

    this->board = board;

    for(int i=0;i<size;i++)
    {
        //origin.first += 10;origin.second += 10;
        emit addPoint(origin.first, origin.second);
        this->points.push_back(origin);
    }

    this->generateDestination();
    this->missingSteps = 0;
}

void Snake::generateDestination()
{
    Point destination;
    int missingSteps;
    do{
        missingSteps = random()%15 + 2;
        //destination.first = rand()%3 - 1;
        //destination.second = rand()%3 - 1;
        destination = this->movAllowed[rand()%4];
    }while(!isValidDestination(this->destination, destination, missingSteps));

    this->missingSteps = missingSteps;
    this->destination = destination;
}

bool Snake::isValidDestination(Point current, Point now, unsigned int missingSteps)
{
    if(now.first == 0 && now.second == 0)return false;
    if((current.first + now.first == 0) && (current.second + now.second == 0))return false;

    if((this->points[0].first + (now.first * 10 * missingSteps)) < 10)return false;
    if((this->points[0].first + (now.first * 10 * missingSteps)) > 640)return false;
    if((this->points[0].second + (now.second * 10 * missingSteps)) < 10)return false;
    if((this->points[0].second + (now.second * 10 * missingSteps)) > 490)return false;

    return true;
}

bool Snake::isCollision()
{
    Point newP = this->movePoint(this->points[0], this->destination);

    for(int i=0;i<this->size;i++)
    {
        if(newP == this->points[i])return true;
    }
    return false;
}

void Snake::forward()
{
    if(this->missingSteps == 0)this->generateDestination();

    while(this->isCollision())this->generateDestination();

    Point previous = this->points[0];
    Point tmp;
    this->points[0] = this->movePoint(this->points[0], this->destination);
    emit addPoint(this->points[0].first,this->points[0].second);
    (*this->board)[this->points[0].first][this->points[0].second].lock();

    for(int i=1;i<this->size; i++)
    {
        tmp = this->points[i];
        this->points[i] = previous;
        emit addPoint(this->points[i].first,this->points[i].second);
        previous = tmp;
    }

    this->missingSteps--;
    emit delPoint(previous.first, previous.second);
    (*this->board)[previous.first][previous.second].unlock();
}

Point Snake::movePoint(Point current, Point destination)
{
    current.first = current.first + (destination.first*10);
    current.second = current.second + (destination.second*10);

    return current;
}

void Snake::run()
{
    while(true)
    {
        this->forward();
        this->msleep(100);
    }
}
