#ifndef SNAKE_H
#define SNAKE_H

#include "includes.h"
#include <QThread>
#include <mutex>
#include <map>

class Snake : public QThread
{
    Q_OBJECT
public:
    explicit Snake(map<int,map<int,mutex> > * &board,QObject *parent = 0);
    void forward();
    void run();

signals:
    void addPoint(int x, int y);
    void delPoint(int x, int y);

private:
    unsigned int size;
    Points points;
    unsigned int missingSteps;
    Point destination;
    void generateDestination();
    bool isValidDestination(Point current, Point now, unsigned int missingSteps);
    Point movePoint(Point current, Point destination);
    bool isCollision();
    map<int,map<int,mutex> > * board;
    Points movAllowed = {make_pair(-1,0),make_pair(0,-1),make_pair(0,1),make_pair(1,0)};
};

#endif // SNAKE_H
